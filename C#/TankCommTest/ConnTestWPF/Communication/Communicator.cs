﻿using ConnTestWPF;
using ConnTestWPF.Data;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace TankCommTest.Communication
{
    public enum CommandType { SETSPEED = 1, SETROTATION = 2, SHOOTBULLET = 3, SHOOTROCKET = 4 }
    public class Communicator
    {
        const string ServerIP = "10.4.11.24";
        const int ServerPort = 11111;

        const int MyPortUpperHexa = 72;
        const int MyPortLowerHexa = 54;

        int MyPort;
        UdpClient listenerClient;
        UdpClient senderClient;
        byte[] lastRecieved;
        ViewModel connectedVM;
        MainWindow MainWindowObj;
        DataHandler DataHandler;
        readonly byte PortHigh;
        readonly byte PortLow;

        public bool FirstLoginDone { get; set; }

        public DateTime LastTimeDataRecieved { get; private set; }
        //DispatcherTimer stopTimer = new DispatcherTimer();

        public Communicator(ViewModel vm, MainWindow MainWindowObj, DataHandler DataHandler)
        {
            this.DataHandler = DataHandler;
            this.MainWindowObj = MainWindowObj;
            MyPort = Convert.ToInt32(MyPortUpperHexa.ToString() + MyPortLowerHexa.ToString(), 16);
            listenerClient = new UdpClient(MyPort);
            StartRecieving();
            connectedVM = vm;

            //stopTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            //stopTimer.Tick += StopTimer_Tick;

            PortHigh = Convert.ToByte(MyPortUpperHexa.ToString(), 16);
            PortLow = Convert.ToByte(MyPortLowerHexa.ToString(), 16);
        }



        public void StartRecieving()
        {
            Task.Run(() =>
            {
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = new byte[16];
                while (true)
                {
                    data = listenerClient.Receive(ref sender);
                    LastTimeDataRecieved = DateTime.Now;
                    if (lastRecieved == null)
                        lastRecieved = data;

                    if (BoolArrayEquals(data, lastRecieved))
                    {
                        continue;
                    }
                    lastRecieved = data;

                    if (connectedVM.NeedConsoleLog)
                    {
                        Console.Write(DateTime.Now + " ");
                        PrintByteArray_HEX(data);
                    }

                     MainWindowObj.Dispatcher.Invoke(() => MainWindowObj.DataHandler.ProcessPacket(data));

                    //Console.WriteLine(Encoding.ASCII.GetString(data, 0, data.Length));
                    //listener.Send(data, data.Length, sender);
                }
            });
        }
        public void SendLOGIN()
        {
            FirstLoginDone = true;
            _sendLoginOrLogout(1);
        }
        public void SendLOGOUT()
        {
            _sendLoginOrLogout(3);
        }
        private void _sendLoginOrLogout(byte itemType)
        {
            senderClient = new UdpClient();
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort);
            senderClient.Connect(endPoint.Address, ServerPort);

            byte[] message = {
                0xFF, 0xFF, 0xFF, 0xFF,
                itemType,
                PortHigh,
                PortLow,
                0, 0, 0, 0, 0, 0, 0, 0, 0
            };

            senderClient.Send(message, message.Length);

            if (itemType == 1)
            {
                Console.WriteLine("{0}: LOGIN message sent.", DateTime.Now);
            }
            else if (itemType == 3)
            {
                Console.WriteLine("{0}: LOGOUT message sent.", DateTime.Now);
            }
        }


        public void SendCommand(CommandType commandType, byte parameter)
        {
            byte[] data = new byte[16];
            data[3] = data[2] = data[1] = data[0] = 0xFF;
            data[4] = 2;
            data[5] = PortHigh;
            data[6] = PortLow;
            data[7] = (byte)commandType;
            data[8] = parameter;
            data[9] = data[10] = data[11] = data[12] = data[13] = data[14] = data[15] = 0;
            SendMessageOnNewTask(data);
        }



        //private void StopTimer_Tick(object sender, EventArgs e)
        //{
        //    SendStopCommand();
        //}
        //public void SendStopCommand()
        //{
        //    ExactPoint MyPosition = DataHandler.Map.GetMyPlayerExactPoint(DataHandler.MyPlayerID);


        //    Debug.Write(string.Format("deltaX:{0}, deltaY:{1}\t", MyPosition.DistanceToNextIntegralX, MyPosition.DistanceToNextIntegralY));

        //    double DistanceToCheck = -1;
        //    if (DataHandler.MyPlayer.Rot == 0 || DataHandler.MyPlayer.Rot == 180) // → ←
        //    {
        //        DistanceToCheck = MyPosition.DistanceToNextIntegralX;
        //    }
        //    else if (DataHandler.MyPlayer.Rot == 90 || DataHandler.MyPlayer.Rot == 270) // ↑ ↓
        //    {
        //        DistanceToCheck = MyPosition.DistanceToNextIntegralY;
        //    }

        //    if (DistanceToCheck < 0.14)
        //    {
        //        if (stopTimer.IsEnabled)
        //            stopTimer.Stop();

        //        SendCommand(CommandType.SETSPEED, 100);
        //        Debug.WriteLine("{0}: Rot:{1} Command: STOP", DateTime.Now, DataHandler.MyPlayer.Rot);
        //    }
        //    else
        //    {
        //        byte wantedSpeed = Math.Max((byte)(DataHandler.MyPlayer.Speed * 0.9 + 100), (byte)110);

        //        SendCommand(CommandType.SETSPEED, wantedSpeed);
        //        Debug.WriteLine("{0}: Command: Speed={1}", DateTime.Now, wantedSpeed);
        //        if (!stopTimer.IsEnabled)
        //            stopTimer.Start();
        //    }


        //}


        void SendMessageOnNewTask(byte[] data)
        {
            Task.Run(() =>
            {
                senderClient.Send(data, data.Length);
            });
        }

        #region HELPERS
        //static void PrintByteArray_DEC(byte[] array)
        //{
        //    for (int i = 0; i < array.Length; i++)
        //    {
        //        Console.Write(array[i].ToString().PadLeft(5));
        //    }
        //    Console.WriteLine();
        //}
        static void PrintByteArray_HEX(byte[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0} ", array[i].ToString("X").PadLeft(3));
            }
            Console.WriteLine();
        }
        static bool BoolArrayEquals(byte[] array1, byte[] array2)
        {
            return Enumerable.SequenceEqual(array1, array2);
        }
        #endregion
    }
}