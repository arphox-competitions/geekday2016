﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SziZso_Tree
{
    class Node
    {
        public static int BiggestDepth = 0;
        public static List<List<Node>> AllDepthNodes = new List<List<Node>>();

        public int Depth { get; set; }
        public Node Parent { get; set; }
        public int Value { get; set; }
        public bool? Sign { get; set; }

        public List<Node> Children { get; set; }

        public Node(Node Parent, int Value, bool? Sign, int Depth)
        {
            this.Parent = Parent;
            this.Value = Value;
            this.Sign = Sign;
            this.Depth = Depth;
            if (Depth > BiggestDepth)
            {
                BiggestDepth = Depth;
                AllDepthNodes.Add(new List<Node>());
            }
            AllDepthNodes[Depth - 1].Add(this);

            Children = new List<Node>();
            GenerateSubNodes();
        }
        private void GenerateSubNodes()
        {
            List<int> KezdoSzamOsztoiCopy = Tree.KezdoSzamOsztoi.ToList();
            KezdoSzamOsztoiCopy.Remove(Value);
            foreach (int oszto in Tree.GetOsztok_ÖnmagátKivéve(Value))
            {
                KezdoSzamOsztoiCopy.Remove(oszto);
            }
            Node TempParent = Parent;
            while (TempParent != null)
            {
                KezdoSzamOsztoiCopy.Remove(TempParent.Value);
                foreach (int oszto in Tree.GetOsztok_ÖnmagátKivéve(TempParent.Value))
                {
                    KezdoSzamOsztoiCopy.Remove(oszto);
                }
                TempParent = TempParent.Parent;
            }

            //Console.Write("Depth={0}\t\t", Depth);
            //Console.Write(Value + "\t");
            //KezdoSzamOsztoiCopy.ForEach(s => Console.Write(s + " "));
            //Console.WriteLine();

            // HA LEVÉL
            if (KezdoSzamOsztoiCopy.Count == 0)
            {
                if (Depth % 2 == 0) // PÁROS
                {
                    Sign = false;
                }
                else // PÁRATLAN
                {
                    Sign = true;
                }
            }

            //Console.ReadKey(true);

            // Gyermek nodok generálása
            foreach (int oszto in KezdoSzamOsztoiCopy)
            {
                Children.Add(new Node(this, oszto, null, Depth + 1));
            }
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            string signString = string.Empty;
            switch(Sign)
            {
                case true: signString = "+"; break;
                case false: signString = "-"; break;
                case null: signString = "?"; break;
            }

            string parentString = string.Empty;
            if (Parent != null)
            {
                parentString = Parent.Value.ToString() + "@" + Parent.Depth;
            }

            return string.Format("{0} ({1}) (d={2}, p={3})", Value, signString, Depth, parentString);
        }
    }
}
