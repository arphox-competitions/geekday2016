function Main() {
	var x = document.getElementById("ta").value.split('\n');
	document.getElementById("result").innerHTML = '';
	for (var i=0; i<x.length; i++) {
		document.getElementById("result").innerHTML += '<DIV><STRONG>' + Verify(x[i]) + '</STRONG><SPAN style="padding-left: 4em;">' + x[i] + '</SPAN></DIV>';
	}
}

function Verify(s) {
	sorig = s;
	sprev = '';
	while ( (s.length > 0) && (sprev != s) ) {
		sprev = s;
		s = Cleaning(s);
	}
	
	if ( s.length == 0) {
		return 'Helyes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		//document.getElementById("result").innerHTML += '<BR/>' + sorig + ' <STRONG>Helyes</STRONG>';
	} else {
		return 'Helytelen';
		//document.getElementById("result").innerHTML += '<BR/>' + sorig + ' <STRONG>Helytelen</STRONG>';
	}
}

function Cleaning(s) {
	s = s.replace(/[^\(\)\[\]\{\}]/g, '');
	s = s.replace(/[\(][\)]/g, '');
	s = s.replace(/[\[][\]]/g, '');
	s = s.replace(/[\{][\}]/g, '');
	return s;
}