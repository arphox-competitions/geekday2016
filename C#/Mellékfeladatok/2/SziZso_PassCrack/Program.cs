﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SziZso_PassCrack
{
    class Program
    {
        const string AngolABC = @"abcdefghijklmnopqrstuvwxyz";
        static void Main(string[] args)
        {
            string bemenet = Console.ReadLine();
            bemenet = bemenet.ToLower();
            string[] reszek = bemenet.Split(':');

            string theKODOLVA = reszek[0].Substring(0, 3);
            int kulcs = FindEltolas(theKODOLVA);

            string[] passwordKodok = reszek[1].Split('-');
            passwordKodok[0] = passwordKodok[0].TrimStart(' ');
            string jelszo = string.Empty;
            for (int i = 0; i < passwordKodok.Length; i++)
            {
                string kodolt = Kodol(passwordKodok[i], kulcs);
                jelszo += GetNumberFromEnglishWord(kodolt);
            }
            Console.WriteLine(jelszo);
        }

        static int FindEltolas(string theKODOLVA)
        {
            for (int i = 1; i < 25; i++)
            {
                string visszafejtett = Kodol(theKODOLVA, i);
                if (visszafejtett == "the")
                {
                    return i;
                }
            }
            return -1;
        }

        static int GetNumberFromEnglishWord(string word)
        {
            switch (word)
            {
                case "zero": return 0;
                case "one": return 1;
                case "two": return 2;
                case "three": return 3;
                case "four": return 4;
                case "five": return 5;
                case "six": return 6;
                case "seven": return 7;
                case "eight": return 8;
                case "nine": return 9;
                default:
                    return -1;
            }
        }


        static string KodolSzar(string titkos_szoveg, int kulcs)
        {
            StringBuilder sb = new StringBuilder();
            int length = titkos_szoveg.Length;
            int inputInt = 0;
            for (int i = 0; i < length; i++)
            {
                //Gyorsabb, ha nem függvényt használok
                inputInt = (int)titkos_szoveg[i];
                inputInt += kulcs;
                sb.Append((char)inputInt);
            }
            return sb.ToString();
        }
        static string Kodol(string nyilt_szoveg, int kulcs)
        {
            nyilt_szoveg = nyilt_szoveg.ToLower();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nyilt_szoveg.Length; i++)
            {
                sb.Append(TitkositCharTombos(nyilt_szoveg[i], kulcs, AngolABC));
            }
            return sb.ToString();
        }
        static char TitkositCharTombos(char c, int kulcs, string collection)
        {
            if (c == ' ') //A szóközt szóközbe transzformálja
                return c;

            kulcs %= collection.Length;
            int vissza = collection.IndexOf(c) + kulcs;

            while (vissza < 0) //alulcsordul
            {
                vissza += collection.Length;
            }
            if (vissza >= collection.Length) //túlcsordul
            {
                vissza %= collection.Length;
            }

            return collection[vissza];
        }
    }
}