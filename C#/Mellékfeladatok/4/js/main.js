function Main() {
	var sorok = document.getElementById("ta").value.split('\n');
	document.getElementById("result").innerHTML = '';
	for (var i=0; i<sorok.length; i+=3) {
		var darabszam = sorok[i].split(' ');
		var szornyek = new Array(parseInt(darabszam[0]));
		var fegyverek = new Array(parseInt(darabszam[1]));
		var tmpszornyek = sorok[i+1].split(' ');
		var tmpfegyverek = sorok[i+2].split(' ');
		for (var k=0; k<parseInt(darabszam[0]); k++) {
			var tmpszorny = tmpszornyek[k].split(':');
			switch (tmpszorny[0]) {
				case 'vizi':
					szornyek[k] = 'v' + padDigits(tmpszorny[1],3);
					break;
				case 'szárazföldi':
					szornyek[k] = 's' + padDigits(tmpszorny[1],3);
					break;
				case 'légi':
					szornyek[k] = 'l' + padDigits(tmpszorny[1],3);
					break;
			}
		}
		for (var k=0; k<parseInt(darabszam[1]); k++) {
			var tmpfegyver = tmpfegyverek[k].split(':');
			switch (tmpfegyver[0]) {
				case 'kard':
					fegyverek[k] = 'k' + padDigits(tmpfegyver[1],3);
					break;
				case 'szigony':
					fegyverek[k] = 's' + padDigits(tmpfegyver[1],3);
					break;
				case 'íj':
					fegyverek[k] = 'i' + padDigits(tmpfegyver[1],3);
					break;
			}
		}
		
		szornyek.sort().reverse();
		fegyverek.sort().reverse();
		
		for (var j=0; j<fegyverek.length; j++) {
			switch (fegyverek[j][0]) {
				case 'k':
					for (var k=0; k<szornyek.length; k++) {
						if ( szornyek[k][0] == 's' ) {
							var strong = parseInt(fegyverek[j].substr(1)) - parseInt(szornyek[k].substr(1));
							if ( strong >= 0 ) {
								fegyverek[j] = 'k' + strong;
								szornyek[k] = '';
							} else {
								continue;
							}
						}
					}
					break;
				case 's':
					for (var k=0; k<szornyek.length; k++) {
						if ( szornyek[k][0] == 'v' ) {
							var strong = parseInt(fegyverek[j].substr(1)) - parseInt(szornyek[k].substr(1));
							if ( strong >= 0 ) {
								fegyverek[j] = 's' + strong;
								szornyek[k] = '';
							} else {
								continue;
							}
						}
					}
					break;
				case 'i':
					for (var k=0; k<szornyek.length; k++) {
						if ( szornyek[k][0] == 'l' ) {
							var strong = parseInt(fegyverek[j].substr(1)) - parseInt(szornyek[k].substr(1));
							if ( strong >= 0 ) {
								fegyverek[j] = 'i' + strong;
								szornyek[k] = '';
							} else {
								continue;
							}
						}
					}
					break;
			}
		}
		

		siker = 'SIKER&nbsp;';
		for (var k=0; k<szornyek.length; k++) {
			if ( szornyek[k] != '' ) {
				siker = 'KUDARC';
				break;
			}
		}

		document.getElementById("result").innerHTML += '<DIV>' + (i/3+1) + '. <STRONG>' + siker + '</STRONG><SPAN style="padding-left: 4em;"></SPAN></DIV>';
	}
	
}

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}